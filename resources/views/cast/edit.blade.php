@extends('partial.master')

@section('judul')
Edit data artis
@endsection

@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
@csrf
@method('PUT')
  <div class="form-group">
    <label for="exampleInputEmail1">Name Artis</label>
    <input type="text" name="nama" value="{{$cast->nama}}" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan nama">
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Umur</label>
    <input type="text" name="umur" value="{{$cast->umur}}" class="form-control" placeholder="masukkan umur">
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Bio</label><br>
    <textarea name="bio" value="{{$cast->bio}}"class="form-control" cols="30" rows="10" placeholder="Masukkan bio artis"></textarea>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection