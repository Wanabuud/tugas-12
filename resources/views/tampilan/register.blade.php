<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Sign Up</title>
</head>
<body>
    <div>
        <h1>Buat Account Baru!</h1>
    </div>
        <form action="/welcome" method="post">
        @csrf
            <p>First name:</p>
                <input type="text" name="firstName" id="">
            <p>Last name:</p>
                <input type="text" name="lastName" id="">
            <p>
                Gender:
            </p>
                <input type="radio" name="gender" value="male">
                <label for="male">Male</label> <br>
                <input type="radio" name="gender" value="female">
                <label for="female">Female</label> <br>
                <input type="radio" name="gender" value="other">
                <label for="other">Other</label>
            <p>
                Nationality:
            </p>
            <select name="country" id="">
                <option value="indonesia">Indonesia</option>
                <option value="english">Inggris</option>
                <option value="india">India</option>
                <option value="amerika">Amerika</option>
            </select>
            <p>
                Language Spoken:
            </p>
                <input type="checkbox" name="indonesia" id="">Bahasa Indonesia <br>
                <input type="checkbox" name="english" id="">English <br>
                <input type="checkbox" name="other" id="">Other
            <p>
                Bio:
            </p>
                <textarea name="bio" id="" cols="30" placeholder="masukkan bio" rows="10"></textarea> <br>
              
             <input type="submit" value="Sign Up">
        </form>
</body>
</html>