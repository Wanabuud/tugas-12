<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class form extends Controller
{
    public function bio(){
        return view('tampilan.register');
    }

    public function kirim(Request $request){
        $firstname = $request['firstName'];
        $lastname = $request['lastName'];
        return view('tampilan.welcome', compact('firstname','lastname'));
    }
}
