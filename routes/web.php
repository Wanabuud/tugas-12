<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'dashboard@register');

Route::get('/register', 'form@bio');

Route::post('/welcome', 'form@kirim');

route::get('/table', function(){
    return view('tabel.table');
});

route::get('/data-table', function(){
    return view('tabel.data-table');
});

// CRUD CATEGORY

route::get('/cast', 'castController@index');

route::get('/cast/create', 'castController@create');

route::post('/cast', 'castController@store');

route::get('/cast/{cast_id}', 'castController@show');
route::get('/cast/{cast_id}/edit', 'castController@edit');
route::put('/cast/{cast_id}', 'castController@update');
route::delete('/cast/{cast_id}', 'castController@destroy');